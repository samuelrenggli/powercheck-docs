# 5. Stromproduzenten und –verbraucher

In diesem Kapitel finden Sie Hintergrundinformationen zu den einzelnen Kraftwerktypen und Stromverbraucher.

## Schweizer Stromverbrauch

###### Tabelle 9.1: Jährlicher elektrischer Endenergieverbrauch gemäss Schweizerischer Elektrizitätsstatistik.  
| Jahr | Endverbrauch ( GWh ) |
| :--: | :----------------: |
| 2014 |       57'466       |
| 2015 |       58'246       |
| 2016 |       58'239       |
| 2017 |       58'483       |
| 2018 |       57'647       |
| 2019 |       57'198       |

### Zukünftige Entwicklung
Der Schweizer Verbrauch an eletrischer Energie wird dank genereller Effizienzsteigerungen um nicht viel mehr als 10 % ansteigen. Konkret bedeutet das, dass 2050 ca. 70 - 80 TWh elektrische Energie verbraucht werden. Die zwei zukünftigen grossen, neuen Verbrauchstypen sind:
- __Elektromobilität:__ Der Energiebedarf der Mobilität, welcher bis anhin als grösserer Energiebedarf durch fossile Energieträger gedeckt war, veschiebt sich in Form von Elektromobilität ins Schweizer Stromnetz.
- __Wärmepumpen:__ Der Energiebedarf der Innenraumbeheizung, welcher bis anhin in Form eines grösseren Energiebedarfs durch fossile Energieträger gedeckt war, veschiebt sich durch Nutzung von (effizienten) Wärmepumpen ins Schweizer Stromnetz.
[...weiterlesen](../de/future.md)

## Kernkraftwerke

![Kernkraftwerke](../img/public_Nuclear.jpg)

Kernkraftwerke sind sogenannte Bandenergieerzeuger. Das heisst, dass sie stabil und relativ konstant in ihrem Nennleistungsbereich Strom erzeugen können.

Die Stromerzeugung der Kernkraftwerke bewegte sich in den letzten zehn Jahren zwischen 20 und 26.5 TWh pro Jahr. In den Jahren 2016 und 2017 war die Energiemenge aufgrund einer Revision des KKWs Beznau (Reaktorblock I) verringert.

###### Tabelle 5.1: Schweizer Kernkraftwerke und ihre Leistungen. Quelle: Bundesamt für Energie (BFE) - Schweizerische Elektrizitätsstatistik 2019 (S. 22).

|  Kraftwerk    | Nennleistung (GW) | Hinweis                     |
| :------------ | -----------------: | :-------------------------- |
| Beznau    |              0.365 | 1. Reaktorblock             |
| Beznau    |              0.365 | 2. Reaktorblock             |
| Mühleberg |              0.373 | per 20.12.2019 abgeschaltet |
| Gösgen    |              1.010 |                             |
| Leibstadt |              1.220 |                             |
| **Total**     |          **3.333** |                             |

###### Tabelle 5.2: Erzeugte Strommenge der Schweizer Kernkraftwerke in den letzten Jahren. Quelle: Bundesamt für Energie (BFE) - Schweizerische Elektrizitätsstatistik 2019 (S. 22).

| Jahr | Jahresenergieertrag (GWh) | Nennleistung am 1.1. (GW) |
| :--: | :-------------------: | :---------------: |
| 2014 |        26'370         |       3.333       |
| 2015 |        22'095         |       3.333       |
| 2016 |        20'235         |       3.333       |
| 2017 |        19'499         |       3.333       |
| 2018 |        24'414         |       3.333       |
| 2019 |        25'280         |       3.333       |

In den Jahren 2016 und 2017 war die schweizweit verfügbare Leistung aufgrund einer Revision des KKWs Beznau (Reaktorblock I) verringert. Das erkennt man an den verringerten Jahresenergieerträge 2016 und 2017.

Die installierte Leistung der in PowerCheck hinterlegten Produktionskurven der letzten Jahre stimmt nicht immer exakt mit der veröffentlichten Nennleistung überein, aber der hinterlegt Jahresertrag stimmt mit der Energiestatistik überein.

### Zukünftige Entwicklung
Durch das im Jahre 2018 eingeführte Energiegesetz ist festgelegt worden, dass keine neuen Kernkraftwerke mehr gebaut werden dürfen. Aus diesem Grund wird die Kernenergie in zukünftigen Energiesystemen keine langfristige Rolle mehr spielen. Spätestens 2044 wird das jüngste Kernkraftwerk (Leibstadt) vom Netz gehen. Demzufolge wird die Schweiz 2050 über keine Kernkraftwerke mehr am Netz verfügen. [...weiterlesen](../de/future.md)

## Thermische Kraftwerke

![Thermische Kaftwerke](../img/public_Thermal.jpg)

Die wichtigsten thermischen Kraftwerke sind die 30 Schweizer Kehrichtverbrennungsanlagen (KVA).

###### Tabelle 5.3: Thermische Kraftwerke in der Schweiz (2019). Quelle: eicher+pauli AG im Auftrag des BFE - Thermische Stromproduktion inklusive Wärmekraftkopplung (WKK) in der Schweiz 2019 (S. 3).

| Kategorie                  | WKK* | Anzahl Anlagen | Jahresenergieertrag (GWh) | Anteil |
| :------------------------- | :--: | -------------: | --------------------------: |------: |
| Diverse Stromerzeuger  | nein |             20 |                          11 |  0.3 % |
| KVAs                   | nein |             24 |                       1'825 | 48.4 % |
| KVAs                   |  ja  |              6 |                         521 | 13.8 % |
| Gross-WKK in Industrie |  ja  |             17 |                         388 | 10.3 % |
| Fernheizkraftwerke     |  ja  |             25 |                         490 | 13.0 % |
| Klein-WKK            |  ja  |            859 |                         532 | 14.1 % |
| **Total**                  |      |        **951** |                   **3'768** |        |

\* Wärme-Kraftkopplung (WKK): Gleichzeitige Erzeugung von mechanischer Energie (mit anschliessender Umwandlung in elektrische Energie) und nutzbarer Wärme, beispielsweise für Fernwärmenetze.

#### Keine Gaskraftwerke in der Schweiz

In der Schweiz existieren keine Grossgaskraftwerke. Bei neuen Gaskraftwerken schreibt der Bund eine Wärme-Kraftkopplung vor, bei welcher neben der  Stromerzeugung auch die Wärme genutzt wird.

###### Tabelle 5.4: Erzeugte Strommenge der thermischen Kraftwerke in der Schweiz in den letzten Jahren. Quelle: eicher+pauli AG im Auftrag des BFE - Thermische Stromproduktion inklusive Wärmekraftkopplung (WKK) in der Schweiz 2014/2015/2016/2017/2018/2019 (S. 3).

| Jahr     | Installierte Leistung (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| 2014 |                      0.991 |                     3'567 |
| 2015 |                      1.017 |                     3'521 |
| 2016 |                      0.962 |                     3'899 |
| 2017 |                      0.946 |                     3'864 |
| 2018 |                      0.940 |                     3'727 |
| 2019 |                      0.943 |                     3'768 |

### Zukünftige Entwicklung
Wenn thermische Energie als Energiequelle für die Erzeugung von Strom dient, dann ist aktuell bei 62 % davon Kehricht der Energieträger. Schweizer Kehrichtverbrennungsanlagen (KVAs) erzeugen diesen Strom über Dampfturbinen aus der Verbrennungsabwärme. Insgesamt ist dies bei 30 Schweizer KVAs der Fall. Die restliche elektrische Energie stammt von Fernheizkraftwerken und Gross- und Klein-Wärme-Kraftkopplungsanlagen aus der Industrie, welche vor allem auch für die Erzeugung von Wärme betrieben werden. In dieser Kategorie sind die Energieträger zu 16 % Erdgas, zu 10 % Bio- und Klärgas und zu 7 % Holz. Das ist allerdings keine zukunftsorientierte Energieträger-Zusammenstellung. [...weiterlesen](../de/future.md)

## Solarenergie

![Solarenergie](../img/public_Solar.jpg)

###### Tabelle 5.5: Erzeugte Strommenge aus Photovoltaik in der Schweiz in den letzten Jahren. Quelle: Swisssolar im Auftrag des BFE - Markterhebung Sonnenenergie 2018/2019 (S. 14).
| Jahr     | Nennleistung am 31.12. (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| 2014 |                      1.057 |                       840 |
| 2015 |                      1.390 |                     1'116 |
| 2016 |                      1.660 |                     1'331 |
| 2017 |                      1.902 |                     1'681 |
| 2018 |                      2.168 |                     1'942 |
| 2019 |                      2.492 |                     2'174 |

\* Die historischen Daten der Solarenergie basieren auf schweizweit installierten Photovoltaikanlagen, welche an das Stromnetz angeschlossen sind. Anlagen aus Inselbetrieben werden nicht berücksichtigt, machen aber auch nur einen vernachlässigbar kleinen Teil aus (< 1 %).

Als Faustregel gilt: Jedes Gigawatt installierte Photovoltaik-Leistung produziert in der Schweiz pro Jahr ungefähr 1000 GWh elektrische Energie.

### Zukünftige Entwicklung
Die Energieperspektiven 2050+ des BFE gehen im Szenario "Zero Basis" von einer installierten Leistung im Jahr 2035 von 16.2 GW und im Jahr 2050 von 37.5 GW aus. [...weiterlesen](../de/future.md)

## Windenergie

![Windenergie](../img/public_Wind.jpg)

###### Tabelle 5.6: Erzeugte Strommenge aus Windenergie in der Schweiz in den letzten Jahren. Quelle: eicher+pauli AG im Auftrag des BFE - Schweizerische Statistik der erneuerbaren Energien 2018/2019 (S. 55).

| Jahr     | Nennleistung am 31.12. (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| 2014 |                      0.060 |                       101 |
| 2015 |                      0.060 |                       110 |
| 2016 |                      0.075 |                       109 |
| 2017 |                      0.075 |                       133 |
| 2018 |                      0.075 |                       122 |
| 2019 |                      0.075 |                       146 |

Als Faustregel gilt: Jedes Gigawatt installierte Windkraftwerk-Leistung produziert in der Schweiz pro Jahr knapp 2000 GWh elektrische Energie.

### Zukünftige Entwicklung
Die Energieperspektiven 2050+ des BFE gehen im Szenario "Zero Basis" von einer installierten Leistung im Jahr 2030 von 0.6 GW und im Jahr 2050 von 2.2 GW aus. Eine Windkraftanlage mit 100 m Nabenhöhe hat eine Nennleistung von ungefähr 2 MW. [...weiterlesen](../de/future.md)

## Flusskraftwerke

![Flusskraftwerke](../img/public_River.jpg)

###### Tabelle 5.7: Erzeugte Strommenge aus Flusskraftwerken in der Schweiz in den letzten Jahren. Quellen: Leistungsangaben: Bundesamt für Energie (BFE) - Statistik der Wasserkraftanlagen der Schweiz 2014/2015/2016/2017/2018/2019 (Excel), Energieangaben: Bundesamt für Energie (BFE) - Schweizerische Elektrizitätsstatistik 2019 (S. 13).

| Jahr     |  Nennleistung am 31.12. (GW) | Jahresenergieertrag (GWh) |
| :------- | -------------------------: | ------------------------: |
| 2014 |                       4.83 |                    17'243 |
| 2015 |                       4.87 |                    16'595 |
| 2016 |                       4.91 |                    16'574 |
| 2017 |                       4.94 |                    15'946 |
| 2018 |                       4.99 |                    16'908 |
| 2019 |                       5.01 |                    17'700 |

### Zukünftige Entwicklung
Die Energieperspektiven 2050+ des BFE gehen im Szenario "Zero Basis" von einem geringfügigen Ausbau der Laufwasserkraftwerke (insb. Kleinwasserkraft) aus (von 17'700 GWh im Jahr 2019 auf 18'500 GWh im Jahr 2050). [...weiterlesen](../de/future.md)

