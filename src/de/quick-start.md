# 1. Erste Schritte

In diesen ersten Schritten erfahren Sie, wie Sie ausgehend von einem vordefinierten Szenario ein neues Szenario für die Energielandschaft Schweiz erstellen können. Sie lernen, Resultate darzustellen und Ihr eigenes Szenario mit dem vordefinierten zu vergleichen.



## Schritt 1: Vordefiniertes Szenario berechnen

Wählen Sie aus der Dropdown-Liste ein vordefiniertes Szenario und klicken Sie auf «Berechnung starten». Jetzt werden die Stromflüsse eines Jahres berechnet.

![Ist-Zustand auswählen und berechnen](../img/select_ist.gif)



## Schritt 2: Resultate ansehen

Schauen wir uns erst die Grafiken an, die nun sichtbar sind. Die erste Grafik zeigt beispielsweise die Stromproduktion in einem Jahr, aufgeteilt nach verschiedenen Energiequellen. Die einzelnen Energiequellen können durch Anklicken in der Legende ein- oder ausgeblendet werden. 

![Arbeiten mit Grafiken](../img/production_graph.gif)

Klicken und Ziehen in den Grafiken ermöglicht das Hineinzoomen. Die zeitliche Auflösung kann über die vier Schaltfelder oben rechts eingestellt werden.

Stromproduktion und -verbrauch sind im Netz immer ausgeglichen. Überschüssige Produktion wird den Speichern zugeführt. Sind diese voll, ist der Überschuss entsprechend in den Grafiken gekennzeichnet.


## Schritt 3: Szenario erstellen und vergleichen

Nun wollen wir, ausgehend vom vordefinierten Szenario, ein eigenes Szenario erstellen. Lassen Sie uns die Frage untersuchen, welche Auswirkungen die Elektrifizierung des Verkehrs hat. Wählen Sie dazu in der linken Leiste «Gesamter Stromverbrauch» und erhöhen Sie die Elektrifizierung des Verkehrs mit dem Schieberegler.

![Elektromobilität erhöhen](../img/augment_electromobility.gif)

Speichern Sie nun das Szenario unter anderem Namen und starten Sie die Berechnung. Nun werden die Resultate Ihres Szenarios angezeigt. Klicken Sie nun auf «Vergleichen» und wählen Sie das vordefinierten Szenario aus. Der Szenarien-Vergleich zeigt, welchen Einfluss die Elektrifizierung von 25% des Verkehrs auf das Energiesystem hat.

![Szenarien-Vergleich](../img/comparison.png)





