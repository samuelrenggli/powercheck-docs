// noinspection JSUnusedGlobalSymbols
export default ({ router }) => {
    // noinspection JSUnresolvedFunction
    router.addRoutes([
        { path: '/', redirect: '/de' },
        { path: '/de', redirect: '/de/quick-start.html' },
        { path: '/en', redirect: '/en/quick-start.html' }
    ])
}
